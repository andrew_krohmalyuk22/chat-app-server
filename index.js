const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const PORT = 3000;

app.get('/', function (req, res) {
  res.send('webrtc server')
});

io.on('connection', (socket) => {
  console.log('connected user');

  socket.on('call', (data) => {
    socket.broadcast.emit('call', data);
  });

  socket.on('acceptcall', (data) => {
    socket.broadcast.emit('acceptcall', data);
  });

  socket.on('message', (data) => {
    socket.broadcast.emit('message', data);
  });

  socket.on('offer', (data) => {
    console.log('offer', data);
    socket.broadcast.emit('offer', data);
  });

  socket.on('answer', (data) => {
    console.log('answer', data);
    socket.broadcast.emit('answer', data);
  });

  socket.on('onicecandidate', (data) => {
    console.log('onicecandidate', data);
    socket.broadcast.emit('onicecandidate', data);
  });

  socket.on('disconnect', () => {
    console.log('user disconnected');
  });
});

http.listen(PORT, () => {
  console.log('listening on: 3000');
});
